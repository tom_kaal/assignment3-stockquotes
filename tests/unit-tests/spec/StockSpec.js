/*jslint browser: true, plusplus:true*/

describe("scenario", function () {

    console.log("Ingestelde modus:", app.mode);


    beforeEach(function() {
        app.init();

    });

    afterEach(function(){
        document.body.removeChild(document.querySelector("#container"));
    });

    it("stocks should be defined",function(){
        expect(app.series).toBeDefined();
    });

    it("socket should be defined",function(){
        expect(app.socket).toBeDefined();
    });

    var expectedValue = "Realtime stockquote app";
    var actualvalue = app.initHTML().querySelector("h1").innerText;
    it("It should create DOM elements  for container an page title", function () {
        expect(expectedValue).toBe(actualvalue);
    });
    it("It should verify that app.series is not empty", function () {
        expect(app.series).not.toBe({});
    });
    //only works when the mode is set to local
    if(app.mode === "local") {

        it("It should verify that the table has 25 rows(local only!)(function showdata)", function () {

            expect(app.showData().querySelectorAll("tr").length).toBe(25);


        });
    };

    it("Verify that only letter remain(function createValidCSSNameFromCompany)", function () {
        expect(app.createValidCSSNameFromCompany("*$(*&$c#$%*a%*r%*(")).toBe("car");
    });

    it("Should verify that it gives the min and the max back(function rnd)", function () {
        var maxLoops = 10000, min = -3, max = 3 , hitmin = 0, hitmax = 0, i, r, minFound = false, maxFound =false;

        for( i = 0; i < maxLoops; i++){
            r = app.rnd(min, max);
            if( r === min){
                hitmin++;
                minFound = true;
            }else if( r === max){
                hitmax++;
                maxFound = true;
            }else if(minFound === true && maxFound === true){
           //     console.log("Aantal loops:"+i);
                 i = maxLoops;

            }
        }
        //console.log("hitMin", hitmin);
        //console.log("hitMax", hitmax);

        expect(hitmax).toBeGreaterThan(0);
        expect(hitmin).toBeGreaterThan(0);
    });


    it("Check if the correct date is returned(function getDateFormated) Check first when the correct date is put in the test. If needed adjust manualy. ", function () {
        var today = "03/24/2015"

        expect(app.getDateFormated()).toBe(today);
    });

    it("Check if the correct time is returned(function getTimeFormated)Check first when the correct time is put in the test. If needed adjust manualy", function () {
        var now = "3:32 PM"
        expect(app.getTimeFormated()).toBe(now);
    });

    it("socket should be connected to http://server7.tezzt.nl:1333",function(){
        expect(app.socket.io.uri).toBe("http://server7.tezzt.nl:1333");
    });

    if(app.mode === "local") {
        it("Check if system really does execute in local mode. The app.modusCheck should be the same as the mode defined (app.modusCheck is set to local in function it should execute in this mode)", function () {
            expect(app.modusCheck).toBe("local");
        });
    };

    if(app.mode === "ajax") {
        it("Check if system really does execute in ajax mode. The app.modusCheck should be the same as the mode defined (app.modusCheck is set to ajax in function it should execute in this mode)", function () {
            expect(app.modusCheck).toBe("ajax");
        });
    };

    if(app.mode === "sockets") {

        it("Check if system really does execute in sockets mode. The app.modusCheck should be the same as the mode defined (app.modusCheck is set to sockets in function it should execute in this mode)", function () {
            expect(app.modusCheck).toBe("sockets");
        });
    };


    it("Check if system really is set to a correct mode(local,ajax or sockets ",function(){
        expect(app.selectedMode(app.mode)).toBe(true);
    });

    it("should exist",function(){
        expect(app.settings).toBeDefined();
    });

    it("should have a valid refresh",function(){
        expect(app.settings.refresh).toEqual(jasmine.any(Number));
        expect(app.settings.refresh).toEqual(1000);
    });

    it("should have a valid ajaxUrl",function(){
        expect(app.settings.ajaxUrl).toEqual(jasmine.any(String));
        expect(app.settings.ajaxUrl).toEqual("http://server7.tezzt.nl/~theotheu/stockquotes/index.php");
    });

});


