/*global app*/
/*global data*/
/*global  document*/
/*global console*/
/*global window*/
/*global io*/
/*global XMLHttpRequest*/
/*global setTimeout*/
/*global container*/
(function () {
    "use strict";

    window.app = {

        settings: {
            refresh: 1000,
            ajaxUrl: "http://server7.tezzt.nl/~theotheu/stockquotes/index.php"

        },

        series: {},

        socket: io("http://server7.tezzt.nl:1333"),

        // kies local, sockets of ajax
        mode: "ajax",

        modusCheck: "",


        parseData: function (rows) {
            var company, i;
            for (i = 0; i < rows.length; i += 1) {
                company = rows[i].col0;

                if (app.series[company]) {
                    app.series[company].push(rows[i]);
                } else {
                    app.series[company] = [rows[i]];
                }

            }
            console.log(app.series);

        },

        retrieveData: function () {
            var xhr;
            xhr = new XMLHttpRequest();
            xhr.open('GET', app.settings.ajaxUrl, true);
            xhr.addEventListener("load", app.getJSONString);
            xhr.send();
            // KOMT IN DE TOETS!!!
            app.modusCheck = "ajax";

        },

        getJSONString: function (e) {
            var responseText = e.target.responseText; //,rows
            app.parseData(JSON.parse(responseText).query.results.row);


            //// Dispatch build event
            //app.quoteNode.dispatchEvent(app.event);
        },

        createValidCSSNameFromCompany: function (str) {
            //regular experion to remove everything
            // that is not out of A-z 0-9
            return str.replace(/[^a-zA-Z_0-9-]/g, "");
        },

        getRealTimeData: function () {
            app.socket.on('stockquotes', function (data) {
                //  console.log(data.query.results.row);
                app.parseData(data.query.results.row);

            });
            app.modusCheck = "sockets";

        },

        showData: function () {
            //  console.log("showdata");
            //return value is a dom

            var company, propertyValue, propertyName, table, row, quote, cell;

            table = document.createElement("table");

            //create header

            for (company in app.series) {
                quote = app.series[company][app.series[company].length - 1];

                row = document.createElement("tr");
                row.id = app.createValidCSSNameFromCompany(company);

                // console.log(quote.col4)
                if (quote.col4 < 0) {
                    // console.log("negatief");
                    row.className = "loser";
                } else if (quote.col4 > 0) {
                    // console.log("positve");
                    row.className = "winner";

                }
                // create cells
                table.appendChild(row);

                // iterate over quote to cells
                for (propertyName in quote) {
                    propertyValue = quote[propertyName];
                    cell = document.createElement("td");
                    cell.innerText = propertyValue;
                    row.appendChild(cell);
                }


            }
            // console.log(table);

            return table;

        },

        loop: function () {

            if (app.mode === "local") {

                app.generateTestData();
                app.modusCheck = "local";
            }
            document.querySelector("#container").removeChild(document.querySelector("table"));
            console.log("nog een keer");
            document.querySelector("body").appendChild(container);

            container.appendChild(app.showData());

            if (app.mode === "ajax" && app.selectedMode(app.mode) === true) {
                app.retrieveData();
            }
            setTimeout(app.loop, app.settings.refresh);
        },


        generateTestData: function () {
            //for every app.series, create a new quote
            var company, quote, newQuote;

            for (company in app.series) {
                quote = app.series[company][0];
                newQuote = Object.create(quote);
                newQuote.col1 = company;
                newQuote.col2 = app.getDateFormated(); // new day
                newQuote.col3 = app.getTimeFormated(); // new time
                newQuote.col4 = app.rnd(-5, 5); // difference of price value between this one and the previous quote

                app.series[company].push(newQuote);
            }
        },


        getDateFormated: function () {

            var today, dd, mm, yyyy;
            today = new Date();
            dd = today.getDate();
            mm = today.getMonth() + 1; //January is 0!

            yyyy = today.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            today = mm + '/' + dd + '/' + yyyy;
            return today;

        },
        getTimeFormated: function () {
            // return am/pm
            var d, h, m, amORpm, time;
            d = new Date();
            h = d.getUTCHours();
            m = d.getUTCMinutes();
            if (h > 12) {
                amORpm = "PM";
                h = h - 12;
            } else {
                amORpm = "AM";
            }
            time = h + ":" + m + " " + amORpm;
            return time;

        },

        rnd: function (min, max) {
            // stock prices -%20 en +%20. dus 80% of 120%
            var returnValue = Math.random() * (max - min + 1) + min;
            returnValue = Math.round(returnValue * 100) / 100;

            return returnValue;

        },

        init: function () {


            var container;

            container = app.initHTML();

            document.querySelector("body").appendChild(container);
            if (app.mode === "local" && app.selectedMode(app.mode) === true) {
                app.parseData(data.query.results.row);

            }
            container.appendChild(app.showData());
            if (app.mode === "sockets" && app.selectedMode(app.mode) === true) {
                app.getRealTimeData();
            }
            app.loop();

            // remoce old table



        },

        initHTML: function () {
            var container, h1Node;

            container = document.createElement("div");
            container.id = "container";

            app.container = container;

            h1Node = document.createElement("h1");
            h1Node.innerText = "Realtime stockquote app";

            app.container.appendChild(h1Node);

            return app.container;

        },

        selectedMode: function (mode) {

            if (mode === "local" || mode === "ajax" || mode === "sockets") {
                // mode set correctly
                return true;
            }
        }
    };
}());

